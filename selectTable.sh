#!/bin/bash

selectTable(){

read -p "Enter Table Name: " tableName;
    if ! [[ -f ./database/$dbName/$tableName ]];then
          echo "Table $tableName doesn't exist"
		selectTable
else	
	
select v in "Select all"  "Select Row"
    do
      case $REPLY in
	
	#echo for new line after cat
        1) cat ./database/$dbName/$tableName  ; echo

           break;;
        2) read -p "Enter primary key of the row that you want to select: " value
	      #echo $(sed -n '/$value/p' ./database/$dbName/$tableName)
              #echo $(cat ./database/$dbName/$tableName | grep '$value')    
              res=$(awk 'BEGIN{FS="|";RS="\n"}{{for(i=1;i<=NF;i++){if($i=="'$value'") print $0}}}' ./database/$dbName/$tableName)
              if [[ ! $res == "" ]];then
	    	 echo $res   
	      else
	    	 echo "Invalid input"
	      fi	      
              break;;

      esac
    done

fi
}
