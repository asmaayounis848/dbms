#!/bin/bash

createTable(){

   sep="|"
   rowSep="\n"
   pK=""
   count=1
 
read -p "Please enter Table Name: " tableName

#check for name validation and empty      
 if [[ ! $tableName =~  ^[a-zA-Z]+[a-zA-Z0-9]*$ ]] || [[ $tableName == '' ]]
    then
        echo "Name not valid"
 #check if table already exists       
 elif [  -f ./database/$dbName/$tableName ]
        then
            echo "try again table already exist"
        else
       
read -p  "Number of Columns: " colnum
#col count validation
if [[ $colnum =~  ^[0-9]*$ ]] && [[ ! $colnum == '' ]] && [[ $colnum -lt 50 ]]
    then   
	while [ $count -le $colnum ]
  do
    read -p  "Name of col number $count: " colname

    echo "Type of col $colname: "
    select v in "integer" "string"
    do
      case $v in
        integer) 
            colType="integer";
            break
            ;;
        string) 
            colType="string";
                break
                ;;
	 *) echo "Invalid input" ;;
      esac
    done
       
    if [[ $pK == "" ]]; then
      echo -e "Make it PrimaryKey ? "
      select var in "yes" "no"
      do
        case $var in
          yes) pK="PK";
          metaData+=$rowSep$colname$sep$colType$sep$pK;
          break;;
          no)
          metaData+=$rowSep$colname$sep$colType$sep""
          break;;
          * ) echo "Invalid input" ;;
        esac
      done
    else
      #user already choose PK	
      metaData+=$rowSep$colname$sep$colType$sep""
    fi


    ((count++))
    #metadata file
    touch  $tableName
    #table data file
    touch ./database/$dbName/$tableName
    
    #add col name to table data file   
    echo -n $colname"|" >> ./database/$dbName/$tableName
      
    done
    
#add metadata    
echo -e $metaData  >> $tableName
	
echo "Table has been created successfully"
else
echo "Invalid input"	
fi  
fi


}
