#!/bin/bash
source ./createDB.sh
source ./listDB.sh
source ./connectToDB.sh
source ./dropDB.sh

PS3="DBMS>> "

#check if DB dir already exists if not create one  
if [[ ! -d ./database ]]
 then
    mkdir ./database
fi

echo "welcome to DBMS😇"

echo "Please enter your choice"

#select case for user choice 
select v in "Create DB" "List DB" "Connect To DB" "Drop DB" "Exit"

do
  
    case $REPLY in
        1)

        createDB
	
        ;;
        
        2)
        
        listDB
	;;
		  
        3)
        
	read -p "Please Enter database name: "  dbName
	
	if [[ -d database/$dbName ]]
	then
		#send dbName to other scripts
		export dbName
		connectToDB
	else 
		echo "Database doesn't exist, Please create it."
	fi
        
        ;;
        
        4)
        
	dropDB
	
        ;;
        
        5)
    

	echo "Good buy✋..."
	exit 
	
        ;;
        
        *) 
		
	echo "Invalid input"
	;;
        

    esac
    
done
