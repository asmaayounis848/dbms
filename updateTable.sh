#!/bin/bash

function updateTable() {

read -p "Please enter table name: " tableName;
    if ! [[ -f ./database/$dbName/$tableName ]];then
          echo "Table $tableName doesn't exist"
          updateTable
else 
  read -p "Enter Column name: " colName;
  #search in first row for col number
  colNum=$(awk 'BEGIN{FS="|"}{if(NR==1){for(i=1;i<=NF;i++){if($i=="'$colName'") print i}}}' ./database/$dbName/$tableName)
  
  echo "col num" $colNum
  if [[ $colNum == "" ]]
  then
    echo "Column Not Found"
    
  else
    read -p "Enter filed Value you want to change: " oldVal;
    
    echo "------------- old value" $oldVal
    
    #serach for old value if it exists in the given col
    res=$(awk 'BEGIN{FS="|"}{if ($'$colNum'=="'$oldVal'") print $'$colNum'}' ./database/$dbName/$tableName)
     echo "------------- res" $res
    if [[ $res == "" ]]
    then
      echo "Value Not Found"
      
    else
        read -p "Enter new Value to set: " newValue;
        NR=$(awk 'BEGIN{FS="|"}{if ($'$colNum' == "'$oldVal'") print NR}' ./database/$dbName/$tableName)
        sed -i ''$NR's/'$oldVal'/'$newValue'/' ./database/$dbName/$tableName 
        echo "Row Updated Successfully"      
    fi
  fi
fi  
}
