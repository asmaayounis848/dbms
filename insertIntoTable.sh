#!/bin/bash

insertIntoTable(){
row=""
sep="|"
rowSep="\n"

    read -p "Enter Table Name: " tableName;
    if ! [[ -f ./database/$dbName/$tableName ]];then
          echo "Table $tableName doesn't exist"

else
  #check metadata file for no. of column
  colnum=$(cat $tableName | wc -l)
  
  for (( i = 2; i <= $colnum; i++ )); do
    colName=$(awk 'BEGIN{FS="|"}{ if(NR=='$i') print $1}' $tableName)
    colType=$( awk 'BEGIN{FS="|"}{if(NR=='$i') print $2}' $tableName)
    colKey=$( awk 'BEGIN{FS="|"}{if(NR=='$i') print $3}' $tableName)
    echo -e "$colName($colType) = \c"
    read data


    # datatype validate
  if [[ $colType == "integer" ]]; then
      while ! [[ $data =~ ^[0-9]*$ ]]; do   
        echo -e "invalid DataType !!"
        echo -e "$colName ($colType) = \c"
        read data
      done
  fi


  if [[ $colType == "string" ]]; then
      while ! [[ $data =~ ^[a-zA-Z]+$ ]]; do   
        echo -e "invalid DataType !!"
        echo -e "$colName ($colType) = \c"
        read data
      done
 fi
 
    #Set row
    if [[ $i == $colnum ]]; then
     row=$rowSep$row$data
   else
     row=$row$data$sep
   fi
 done
  echo -e $row"\c" >> ./database/$dbName/$tableName

  if [[ $? == 0 ]]
  then
    echo "Data Inserted Successfully"
  else
    echo "Error Inserting Data into Table $tableName"
  fi

fi

}
