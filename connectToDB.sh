#!/bin/bash

source ./createTable.sh
source ./listAllTables.sh
source ./dropTable.sh
source ./selectTable.sh
source ./insertIntoTable.sh
source ./updateTable.sh
source ./deleteRow.sh


connectToDB(){
export dbName
select c in "Create Table" "List All Tables" "Drop Table" "Select From Table" "Insert into Table"  "Delete From Table" "Update Table"  "Back"
      
do        
            case $REPLY in 
		
		1)
		
		createTable
		;;
		
		2)
	
		listAllTables
    		;;
    		
		3)
	
		dropTable
		;;
		
             	4)

                selectTable
		;;

	        5)
	
	        insertIntoTable
		;;
		
                6)
                
		deleteRow 
		;;
            
		7)
	
                updateTable
		;;
            
		8) 
		
		clear
		#exit from this list to DB list
		./dbStart.sh
		exit
		;;
		
		*) 
		
		echo "Invalid input"
		;;
  	 esac
done


}
