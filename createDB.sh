#!/bin/bash

createDB(){

read -p "Please Enter DataBase name: " dbNAme

#check for database name validation
if [[ ! $dbNAme =~  ^[a-zA-Z]+[a-zA-Z0-9]*$ ]] || [[ $dbNAme == '' ]];
	then echo "Please try again, not valid input"
		createDB
	elif [ -d ./database/$dbNAme ]; then
		echo "Name exists, Please Try again"
		createDB
else
	mkdir ./database/$dbNAme
	echo "$dbNAme DataBase created successfuly"
fi

}
