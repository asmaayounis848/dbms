#!/bin/bash

dropDB(){

read -p "Please Enter database name to delete: " dbName
if [ -d ./database/$dbName ]
        then
	    #confirmation to drop DB
            echo "Are You Sure You Want To drop $dbName ? (Y,N)"
            read answer
            
	       if [[ $answer == [Yy] ]]
               then rm -r ./database/$dbName
		echo "database deleted successfully"
	        
                    elif [[ $answer == [Nn] ]]
                      then 
			echo "cancel"
		      #if answer isn't yes or no	
                      else 
                      echo "$answer is invalid"
                   fi
            
	       else
                echo "database not found"       
     		dropDB
		clear

fi 
}
